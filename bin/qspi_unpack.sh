#!/bin/sh -eu
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright 2019 (C) Olliver Schinagl <oliver@schinagl.nl>
#

set -eu

FLASH_BLOCK_SIZE=64
FLASH_FSBL_OFFSET="0x00"
FLASH_FSBL_SIZE="0x0036e900"
FLASH_UBOOT_OFFSET="0x0036e900"
FLASH_UBOOT_SIZE="0x00061bb0"
KEEPGELDIR=0
QSPIDIR_TEMPLATE="qspiunpacker"


usage()
{
    echo "Uage: ${0} [OPTIONS] FILE"
    echo "    -h  Print usage"
    echo "    -k  Keep temporary extracted files"
    echo "    -o  Output directory"
}

cleanup()
{
    if [ -d "${QSPIDIR}" ] && [ -z "${QSPIDIR##*${QSPIDIR_TEMPLATE}*}" ]; then
        if [ "${KEEPGELDIR}" -eq 1 ]; then
            echo "Not deleting '${QSPIDIR}', please remove manually."
        else
           rm -rf "${QSPIDIR:?}/"
        fi
    fi
}

main()
{
    while getopts ":hko:" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        k)
            KEEPGELDIR=1
            ;;
        o)
            OUTDIR="${OPTARG}"
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    if [ ! -f "${1}" ]; then
        echo "ERROR: Missing QSPI dump file."
        usage
        exit 1
    fi

    QSPIDIR="$(mktemp -d -t "${QSPIDIR_TEMPLATE}.XXXXXXXX")"
    if [ -z "${OUTDIR:-}" ]; then
        OUTDIR="${QSPIDIR}"
    fi
    mkdir -p "${OUTDIR}"

    dd \
        bs="${FLASH_BLOCK_SIZE}" \
        count="$(printf %d "$((FLASH_FSBL_SIZE / FLASH_BLOCK_SIZE))")" \
        if="${1}" \
        of="${OUTDIR}/FSBL" \
        skip="$(printf %d "$((FLASH_FSBL_OFFSET / FLASH_BLOCK_SIZE))")"

    dd \
        bs="${FLASH_BLOCK_SIZE}" \
        count="$(printf %d "$((FLASH_UBOOT_SIZE / FLASH_BLOCK_SIZE))")" \
        if="${1}" \
        of="${OUTDIR}/uboot.bin" \
        skip="$(printf %d "$((FLASH_UBOOT_OFFSET / FLASH_BLOCK_SIZE))")"

    echo "Successfully unpacked QSPI flash dump '${1}'."
}

trap cleanup EXIT
main "${@}"

exit 0
